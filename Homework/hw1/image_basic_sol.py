import cv2
import numpy as np

def load(image_path):
    """
    Loads an image from a file path
    Args:
        image_path: file path to the image
    Returns:
        out: numpy array of shape(image_height, image_width, 3)
    """

    out = None

    #### YOUR CODE HERE ####
    out = cv2.imread(image_path)
    #### END YOUR CODE #####

    return out

def bgr2rgb(img):
    """
    Converts an image from BGR format to RGB format
    Args
        img: a numpy array, image in BGR format
    Return
        out: image in RGB format
    """

    out = None

    #### YOUR CODE HERE ####
    out = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    #### END YOUR CODE #####

    return out

def convert_to_grey_scale(img):
    """
    Converts an image from BGR format to grey scale
    Args:
        img: a numpy array, image in BGR format
    Returns:
        out: grey image
    """

    out = None

    #### YOUR CODE HERE ####
    out = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    #### END YOUR CODE #####

    return out

def bgr_decomposition(image, channel):
    """
    Return image **excluding** the rgb channel specified
    Args:
        image: numpy array of shape(image_height, image_width, 3)
        channel: str specifying the channel
    Returns:
        out: numpy array of shape(image_height, image_width, 3)
    """

    out = None

    ### YOUR CODE HERE
    color = {'b': 0, 'g': 1, 'r': 2}
    image = np.array(image)
    image[..., color[channel.lower()]] = 0

    out = image
    ### END YOUR CODE

    return out

def convert_to_hsv(img):
    """
    Converts an image from BGR format to HSV color sp
    Argsace
        img: a numpy array, image in BGR format
    Return
        out: HSV image
    """

    out = None

    #### YOUR CODE HERE ####

    out = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

    #### END YOUR CODE #####

    return out
