import numpy as np
import cv2

def sift_extract(img):
    '''
    Sift features extraction.
    Require: Implement below all the comment with "(implement!)"
    Args:
        img: gray image
    Returns:
        img_keypoints: image with keypoints drawn in.
    '''

    #### YOUR CODE HERE ####

    # Define SIFT object in OpenCV (implement!)

    # Get keypoints from img (implement!)

    # Clone original image for drawing
    img_keypoints = img.copy()

    # Draw keypoint in image (implement!)

    #### END YOUR CODE #####

    return img_keypoints


def sift_matching(img1, img2):
    '''
    Sift features extraction.
    Require: Implement below all the comment with "(implement!)"
    Args:
        img1: gray image
        img2: gray image
    Returns:
        img_matching: image with matching features in two input images.
    '''

    img_matching = None

    #### YOUR CODE HERE ####

    # Define SIFT object in OpenCV (implement !)

    # Get keypoints and descriptors with SIFT from two images (implement !)
    kp1, des1 = None, None
    kp2, des2 = None, None

    # Define BFMatcher object (implement !)
    bf = None
    matches = None

    # Apply ratio test
    good = []
    for m, n in matches:
        if m.distance < 0.75 * n.distance:
            good.append([m])

    # cv.drawMatchesKnn expects list of lists as matches (implement !)

    #### END YOUR CODE #####


    return img_matching