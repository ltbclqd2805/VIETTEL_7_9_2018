import numpy as np
import cv2

def kmeans_segmentation(img, num_clusters, max_iter=50, expected_acc=1.0, attempts=10):
    '''
    Image segmentation using kmeans clustering.
    Require: stop the iteration when any of the below condition is met:
        i) stop the algorithm iteration if specified accuracy, expected_acc, is reached.
        ii) stop the algorithm after the specified number of iterations, max_iter

    Args:
        img: numpy array of shape(image_height, image_width, 3)
        num_clusters: number of clusters
        attempts: Flag to specify the number of times the algorithm is executed using different initial labellings.
        The algorithm returns the labels that yield the best compactness. This compactness is returned as output.

    Returns:
        out: image after using kmeans segmentation
    '''

    # Reshape image
    Z = img.reshape((-1, 3))
    Z = np.float32(Z)

    # Define args of cv2.kmeans method
    ######### YOUR CODE HERE ##########

    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 10, 1.0)
    ret, label, center = cv2.kmeans(Z, num_clusters, None, criteria, 10, cv2.KMEANS_RANDOM_CENTERS)

    ######### END CODE HERE ###########

    center = np.uint8(center)
    res = center[label.flatten()]
    res2 = res.reshape((img.shape))

    return res2