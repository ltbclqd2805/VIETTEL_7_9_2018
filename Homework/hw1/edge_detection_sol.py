import cv2
import numpy as np


def sobel_edge_detection(img, direction=1, ksize=3):
    out = None

    ### YOUR CODE HERE ###

    out = cv2.Sobel(img, cv2.CV_64F, direction, 1 - direction, ksize=ksize)

    ### END YOUR CODE ####
    return out

def canny_edge_detection(img, thresh1, thresh2, ksize=3):
    out = None

    ### YOUR CODE HERE ###

    out = cv2.Canny(img, thresh1, thresh2, apertureSize=ksize)

    ### END YOUR CODE ####
    return out


def line_detection(edges, rho, theta, thresh, minLineLength=None, maxLineGap=None):
    '''
    Line detection using HoughLine or HoughLineP.
    Require: If minLineLength = None or maxLineGap = None, uses HoughLine. Otherwise, uses HoughLineP

    Return:
        Return the an array of (rho, theta) values.
    '''

    lines = None

    #### YOUR CODE HERE ####
    if (minLineLength is None) or (maxLineGap is None):
        lines = cv2.HoughLines(edges, rho, theta, thresh)
    else:
        lines = cv2.HoughLinesP(edges, rho, theta, thresh, minLineLength, maxLineGap)

    #### END YOUR CODE #####

    return lines


def circle_detection(img, dp=1, min_dist=10, param1=50, param2=30, minRadius=0, maxRadius=0):
    '''
        Circle detection using Hough Circle

        Args:
            dp: The inverse ratio of resolution
            min_dist: Minimum distance between detected centers
            param1: Upper threshold for the internal Canny edge detector
            param2: Threshold for center detection
            min_radius: Minimum radio to be detected. If unknown, put zero as default.
            max_radius: Maximum radius to be detected. If unknown, put zero as default

        Return:
            Return the an array of circles, which described by (x, y, r) values.
    '''

    out = None

    #### YOUR CODE HERE ####
    out = cv2.HoughCircles(img, cv2.HOUGH_GRADIENT, dp, min_dist,
                               param1=param1, param2=param2, minRadius=minRadius, maxRadius=maxRadius)

    #### END YOUR CODE #####
    return out

