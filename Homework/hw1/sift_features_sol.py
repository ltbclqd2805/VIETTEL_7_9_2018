import numpy as np
import cv2

def sift_extract(img):
    '''
    Sift features extraction.
    Args:
        img: gray image
    Returns:
        img_keypoints: image with keypoints drawn in.
    '''

    #### YOUR CODE HERE ####

    ## Define SIFT object in OpenCV
    sift = cv2.xfeatures2d.SIFT_create()

    ## Get keypoints from img
    kp = sift.detect(img, None)

    ## Clone original image for drawing
    img_keypoints = img.copy()

    ## Draw keypoint in image
    img_keypoints = cv2.drawKeypoints(img, kp, img_keypoints)


    #### END YOUR CODE #####

    return img_keypoints

def sift_matching(img1, img2):
    '''
    Sift features extraction.
    Args:
        img1: gray image
        img2: gray image
    Returns:
        img_matching: image with matching features in two input images.
    '''

    img_matching = None

    #### YOUR CODE HERE ####

    # Define SIFT object in OpenCV
    sift = cv2.xfeatures2d.SIFT_create()

    # Get keypoints and descriptors with SIFT from two images
    kp1, des1 = sift.detectAndCompute(img1, None)
    kp2, des2 = sift.detectAndCompute(img2, None)

    # Define BFMatcher object
    bf = cv2.BFMatcher()
    matches = bf.knnMatch(des1, des2, k=2)

    # Apply ratio test
    good = []
    for m, n in matches:
        if m.distance < 0.75 * n.distance:
            good.append([m])

    # cv.drawMatchesKnn expects list of lists as matches.
    img_matching = cv2.drawMatchesKnn(img1, kp1, img2, kp2, good, img_matching, flags=2)


    #### END YOUR CODE #####


    return img_matching
