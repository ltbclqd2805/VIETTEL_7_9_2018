from sklearn import svm
from sklearn.ensemble import RandomForestClassifier


def split_data(all_data, test_rate):
    train_data = []
    test_data = []

    #### YOUR CODE HERE ####

    thresh = (int)(len(all_data) * (1 - test_rate))
    train_data, test_data = all_data[:thresh], all_data[thresh:]

    #### END YOUR CODE #####

    return train_data, test_data


def svm_classification(xtrain, ytrain):
    clf = None

    #### YOUR CODE HERE ####

    clf = svm.SVC()
    clf.fit(xtrain, ytrain)

    #### END YOUR CODE #####

    return clf


def rf_classification(xtrain, ytrain, n_tree):
    rf = None

    #### YOUR CODE HERE ####
    rf = RandomForestClassifier(n_estimators=n_tree)
    rf.fit(xtrain, ytrain)

    #### END YOUR CODE #####

    return rf
