import os
import cv2


def load_image(image_path):
    #### YOUR CODE HERE ####

    return cv2.imread(image_path)

    #### END YOUR CODE #####


def statistic():
    label = []
    num_images = []

    #### YOUR CODE HERE ####

    for lab in os.listdir('trainingset'):
        label.append(lab)
        num_images.append(len(os.listdir(os.path.join('trainingset', lab))))

    #### END YOUR CODE #####

    return label, num_images

def read_data(label2id):
    X = []
    Y = []

    #### YOUR CODE HERE ####

    for label in os.listdir('trainingset'):
        for img_file in os.listdir(os.path.join('trainingset', label)):
            img = cv2.imread(os.path.join('trainingset', label, img_file))
            X.append(img)
            Y.append(label2id[label])

    #### END YOUR CODE #####

    return X, Y