import os
import numpy as np
import cv2

from sklearn.cluster import KMeans
from scipy.spatial.distance import cdist


def extract_sift_features(X):
    image_descriptors = []

    #### YOUR CODE HERE ####
    sift = cv2.xfeatures2d.SIFT_create()

    for i in range(len(X)):
        kp, des = sift.detectAndCompute(X[i], None)
        image_descriptors.append(des)
    #### END YOUR CODE #####

    return image_descriptors


def kmeans_bow(all_descriptors, num_clusters):
    bow_dict = []

    #### YOUR CODE HERE ####

    kmeans = KMeans(n_clusters=num_clusters).fit(all_descriptors)
    bow_dict = kmeans.cluster_centers_.tolist()

    #### END YOUR CODE #####

    return bow_dict


def create_features_bow(image_descriptors, BoW, num_clusters):
    X_features = []

    #### YOUR CODE HERE ####
    for i in range(len(image_descriptors)):
        features = np.array([0] * num_clusters)

        if image_descriptors[i] is not None:
            distance = cdist(image_descriptors[i], BoW)
            argmin = np.argmin(distance, axis=1)

            for j in argmin:
                features[j] += 1

        X_features.append(features)

    #### END YOUR CODE #####

    return X_features